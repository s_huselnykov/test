package com.benny.annotations;

public interface Coach {

    public String getDailyWorkout();
}
